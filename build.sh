#!/bin/sh
for name in src/*.less; do
	lessc "$name" "output/$(basename "$name" .less).css"
done
